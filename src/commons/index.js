/* Index header */
"use strict";
var $ = require("jquery");

/* STYLE */
var style = require("./header.css");
style.use();

/* TEMPLATE HEADER*/
var header = require("./header.html");
var nav = require("./nav.html");

$("#monapp").append(header);
$("header").after(nav);
