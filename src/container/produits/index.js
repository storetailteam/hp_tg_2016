"use strict";
var $ = require("jquery");
var style = require("./produits.css");
var template = require("./produits.html");
var settings = require("../../settings.json");
style.use();

/* DECLARE IMAGES */
var images = {};
for (var i = 0; i < settings.structure.page.fnames.length; i++) {
    var produit_id = settings.structure.page.produits[i],
        produit = settings.produits[produit_id],
        fname = settings.structure.page.fnames[i];

    images[produit_id] = {
        header:     require('../../assets/img/'+fname+'/img_header.jpg'),
        retour:     require('../../assets/img/'+fname+'/icone_retour.png'),
        signature:  require('../../assets/img/'+fname+'/signature.png'),
        logo_hp:    require('../../assets/img/'+fname+'/logo_hp.png'),
        logo_intel: require('../../assets/img/'+fname+'/logo_intel.png'),
        centre:     require('../../assets/img/'+fname+'/img_centre.jpg'),
        carac1:     require('../../assets/img/'+fname+'/picto_carac1.png'),
        carac2:     require('../../assets/img/'+fname+'/picto_carac2.png'),
        carac3:     require('../../assets/img/'+fname+'/picto_carac3.png'),
        desc1:     require('../../assets/img/'+fname+'/picto_desc1.png'),
        desc2:     require('../../assets/img/'+fname+'/picto_desc2.png'),
        desc3:     require('../../assets/img/'+fname+'/picto_desc3.png'),
        desc4:     require('../../assets/img/'+fname+'/picto_desc4.png'),
        visuelodr:  require('../../assets/img/'+fname+'/visuel_odr.jpg'),
        video:      produit.page.videos.length > 0 ? require('../../assets/img/'+fname+'/videos/video.mp4') : ''
    };
    images[produit_id]['carrousel'] = [];
    for(var j = 0; j < produit.page.carrousel; j++) {
        images[produit_id]['carrousel'].push(require('../../assets/img/'+fname+'/carrousel/carrousel_'+(j+1)+'.jpg'));
    }
};

$("nav").after(template);

export { images };
