"use strict";

/// ------------------- CHARGEMENT DES RESSOURCES -----------------------
var sto = window.__sto;
var Vue = require("vue");
var $ = require("jquery");
var slick = require("./src/assets/js/slick.min.js");
var settings = require("./src/settings.json");

var commons = require("./src/commons/index.js"),
    home    = require("./src/container/home/index.js"),
    produits= require("./src/container/produits/index.js");

var oldHeight = 0;
setInterval(function() {
    var height = $('#monapp').height(), gap = 200;
    if (height !== oldHeight && ((height < oldHeight - gap)||(height > oldHeight))) {
        post("sto_changeHeight", height);
        oldHeight = height;
    }
}, 16);

function setRatioSlide(select){select.css({"height":select.width() / 1.36})}
function setRatioProductSheet(select){select.css({"height":select.width() * 2.23})}
function setSquarePreviewProd(select){select.css({"height":select.width()})}
$( window ).resize(function() {
    setRatioSlide($('#cr_products .w_slide'));
    setRatioProductSheet($('.product_sheet'));
    setSquarePreviewProd($('.product .product-preview'));
});

var all_products = { };

// POST MESSAGES & TRACKING FUNCTIONS
function post(method, value) {
    var result = {"method": method};
    if (value !== undefined){
        result.value = value;
    }
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerMethod(trackingObj) {
    post("sto_tracking", JSON.stringify(trackingObj));
}
function trackerBasket(basket, trackingObj) {
    post("sto_trackbasket", { "basket": JSON.stringify(basket), "value": JSON.stringify(trackingObj) });
}

// SLICK COMPONENT
Vue.component('slick', {
    template: require("./src/container/slick/index.html"),
    props: [ 'name', 'items' ],
    beforeUpdate: function() {
        $('#cr_'+this.name).slick('unslick');
    },
    updated: function() {
        this.initSlick();
    },
    created: function() {
        this.initSlick();
    },
    methods: {
        initSlick: function() {
            setRatioSlide($('#cr_products .w_slide'));
            setRatioProductSheet($('.product_sheet'));
            setSquarePreviewProd($('.product .product-preview'));
            this.$nextTick(function() {
                $('#cr_'+this.name).slick({
                    infinite: true,
                    slidesToShow: 4,
                    dots:false,
                    arrows: false
                });
            })
        },
        slideClick: function(item) {
            this.$emit('slideclick', item);
        },
        prevItem: function() {
            $('#cr_'+this.name).slick('slickPrev');
        },
        nextItem: function() {
            $('#cr_'+this.name).slick('slickNext');
        }
    }
});

// AFFICHAGE DE LA HOMEPAGE
var monapp = new Vue({
    el: '#monapp',
    data: {
        state: 'init',
        page: 'home',
        produit: '',
        params: settings,
        products: {},
        popin_show: false,
        popin_content: '',
        popin_type: 'image'
    },
    beforeMount: function() {
        var t = this, products;

        // GET ALL PRODUCTS
        products = settings.products;

        while(products.length)  {
            $.getJSON('http://mb01.storetail.net/api/fnac/' +products.splice(0, 10).join(';'), function (data) {
                Object.keys(data).forEach(function (v) {
                    // Vérification Dispo
                    if (data[v].timestamp < (Date().now - 8 * 60 * 60))
                        data[v].disponible = false;// Vérification Dispo
                });
                all_products = $.extend(all_products, data);
                if (!products.length) {
                    t.$set(t, 'state', 'loaded');
                }
            });
        }

    },
    methods: {
        menuChange: function(menu, from) {
            // Fire a Navigation Event
            trackerMethod({ "trackAction": "nav", "trackZone": (this.page == 'home' ? 'home' : this.params.produits[this.produit].fname), "trackLabel": (menu == 'home' ? 'home' : this.params.produits[menu].fname)+'_'+from });

            this.$set(this, 'page', (menu == 'home' ? menu : 'page'));
            this.$set(this, 'produit', (menu == 'home' ? '' : menu));

            this.$nextTick(function() {document.getElementById("monapp").scrollIntoView();});
        },

        clickPDF: function(produit) {
            // Fire a PDF
            trackerMethod({ "trackAction": "clk", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "pdf", "trackLabel": produit });
        },

        clickConfigs: function(produit) {
            // Fire a Toutes les Configs
            trackerMethod({ "trackAction": "clk", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "conf", "trackLabel": produit });
        },

        closePopin: function() {
            if (this.popin_type == 'video') {
                document.getElementById('video').pause();
            }
            this.$set(this, 'popin_show', false);

            // Fire an Action Event
            trackerMethod({ "trackAction": "act", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "close", "trackLabel": "popin" });
        },

        addToCart: function(produit) {
            // Test if product exist
            if (all_products[produit]) {
                // Add the product to the basket
                post("sto_addtocart", produit);

                // Fire an Add To Basket event
                trackerBasket([{
                    "id": produit,
                    "name": all_products[produit].titre,
                    "price": (all_products[produit].prix_barre ? all_products[produit].prix_barre : all_products[produit].prix),
                    "qty": 1,
                    "promo": true}], {"trackAction": "abk", "trackZone": this.params.produits[this.produit].fname});
            }
        },

        showFiche: function(produit) {
            // Fire a Click Event
            trackerMethod({ "trackAction": "clk", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "fiche", "trackLabel": produit });
        },

        showCaract: function(produit) {
            if($('.produit .w_view-all-btn').attr('toggle') == "false"){
                $('.w_view-all-btn').attr('toggle',"true");
                $('.w_view-all-btn a').html('Fermer les caractéristiques');
                $('.table-specs').show();

                // Fire an Action Event
                trackerMethod({ "trackAction": "act", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "open", "trackLabel": "carrousel" });
            }else{
                $('.w_view-all-btn').attr('toggle',"false");
                $('.w_view-all-btn a').html('Voir toutes les caractéritiques');
                $('.table-specs').hide();

                // Fire an Action Event
                trackerMethod({ "trackAction": "act", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "close", "trackLabel": "carrousel" });
            }
        },

        slideClick: function(item) {
            this.$set(this, 'popin_show', true);

            // Fire an Action Event
            trackerMethod({ "trackAction": "act", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "open", "trackLabel": "popin" });

            if (item.video) {
                this.$set(this, 'popin_type', 'video');
                this.$set(this, 'popin_content', produits.images[item.produit].video);
            }
            else {
                this.$set(this, 'popin_type', 'image');
                this.$set(this, 'popin_content', item.content);
            }
        },
        getImageHome: function(img, indice) {
            return home[img][indice];
        },
        getImageProduit: function(produit, type) {
            return produits.images[produit][type];
        },
        getODR: function(text) {
            if (text) {
                var odr = text.split(' ');
                odr[odr.length - 1] = odr[odr.length - 1]+'<sup>*</sup>';
                odr = odr.reduce(function(a, b) {
                    return a+'<span>'+b+'</span>'
                }, '');

                return odr;
            }
            return '';
        },
        getAccessoires: function(produit) {
            return this.params.produits[produit].page.accessoires.filter(function(e) {
                return (all_products[e]);
            }).map(function(e) {
                return all_products[e];
            });
        },
        getCarrousel: function(produit) {
            var carrousel = [];

            for(var i = 0; i < this.params.produits[produit].page.carrousel; i++) {
                carrousel.push({ produit: produit, content: produits.images[produit].carrousel[i], background: 'background-image:url('+produits.images[produit].carrousel[i]+')', video: this.params.produits[produit].page.videos.indexOf(i) !== -1 });
            }

            return carrousel;
        },

        getStars: function(produit) {
            var note = 0;
            if (all_products[produit] && all_products[produit].note) {
                note = all_products[produit].note * 10
            }

            return 'star S'+(note < 10 ? '0' : '')+note;
        },

        getAvis: function(produit) {
            var avis = 0;
            if (all_products[produit] && all_products[produit].avis) {
                avis = all_products[produit].avis;
            }

            return avis;
        },

        getImage: function(produit) {
            var image = '';
            if (all_products[produit] && all_products[produit].images) {
                image = all_products[produit].images[0]
            }

            return image;
        },

        hasPrice: function(produit) {
            if (all_products[produit]) {
                var prix = all_products[produit].prix_barre ? all_products[produit].prix_barre : all_products[produit].prix;
                if (prix && all_products[produit].disponible) {
                    return true;
                }
            }
            return false;
        },

        getPrice: function(produit, type) {
            if (all_products[produit]) {
                var prix = all_products[produit].prix_barre ? all_products[produit].prix_barre : all_products[produit].prix;
                if (prix) {
                    if (type == 'full') {
                        return (prix ? 'à partir de <span>'+parseFloat(prix).toFixed(2).replace(/./g, function(c, i, a) {
                            return i && c !== "." && ((a.length - i) % 3 === 0) ? ' ' + c : c === '.' ? '<span>€' : c;
                        })+'</span></span>' : '');
                    }
                    else {
                        return (prix ? parseFloat(prix).toFixed(2).replace(/./g, function(c, i, a) {
                            return i && c !== "." && ((a.length - i) % 3 === 0) ? ' ' + c : c === '.' ? '<span>€' : c;
                        })+'</span>' : '');
                    }
                }
            }

            return (type == 'full' ? 'Configuration momentanément indisponible' : '');
        },
        getPrice_home: function(produit, type) {
            console.log(all_products[produit]);
            if (all_products[produit]) {
                if(all_products[produit].disponible) {var prix = all_products[produit].prix_barre ? all_products[produit].prix_barre : all_products[produit].prix;}
                else{
                    var price = this.params.produits[produit].page.default_price;
                    var prix = price.replace(",",".");
                }

                if (prix) {
                    if (type == 'full') {
                        return (prix ? 'à partir de <span>'+parseFloat(prix).toFixed(2).replace(/./g, function(c, i, a) {
                            return i && c !== "." && ((a.length - i) % 3 === 0) ? ' ' + c : c === '.' ? '<span>€' : c;
                        })+'</span></span>' : '');
                    }
                    else {
                        return (prix ? parseFloat(prix).toFixed(2).replace(/./g, function(c, i, a) {
                            return i && c !== "." && ((a.length - i) % 3 === 0) ? ' ' + c : c === '.' ? '<span>€' : c;
                        })+'</span>' : '');
                    }
                }
            }

            return (type == 'full' ? 'Configuration momentanément indisponible' : '');
        }
    }
});
